// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "BulletProjectile.h"
#include "ShooterCharacter.generated.h"

UCLASS()
class UNREALSHOOTER_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// spawn a projectile
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class ABulletProjectile> ProjectileClass;

	//bool for timer, counts 60 seconds, and a couple og timer variables. 
	bool isTimeLeft;
	float timer;
	float endTimer;
	float tempDelta;
	
	
	//ends the game if no time left. 
	void GameOver();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// binds the character functionallity to the input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	// moves forward and backward. well it would, but it worked better not to
	UFUNCTION()
	void MoveForward(float Value);

	//and moves right and left.  SAME
	UFUNCTION()
	void MoveRight(float Value);
	
	// jumps when key is pressed. if it's the right key
	UFUNCTION()
	void StartJump();

	// stops jumping when key is released.
	UFUNCTION()
	void StopJump();


	// fires projectiles.
	UFUNCTION()
	void Fire();

	// shooter camera.
	UPROPERTY(VisibleAnywhere)
	UCameraComponent* ShooterCameraComponent;

	//  arms visible only to this player. currently they are turned off though.
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* FPSMesh;

	// bullet offset from the camera location.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	FVector MuzzleOffset;



};
