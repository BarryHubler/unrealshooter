// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>

// Sets default values
AShooterCharacter::AShooterCharacter()
{
 	// Set this character to call Tick() every frame.
	PrimaryActorTick.bCanEverTick = true;
	
	// first person camera component.
	ShooterCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	check(ShooterCameraComponent != nullptr);

	// attach the camera to the capsule component.
	ShooterCameraComponent->SetupAttachment(CastChecked<USceneComponent, UCapsuleComponent>(GetCapsuleComponent()));
	
	// Position the camera slightly above the eyes.
	ShooterCameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 50.0f + BaseEyeHeight));

	// enable camera rotation.
	ShooterCameraComponent->bUsePawnControlRotation = true;

	//  first person mesh 
	FPSMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonMesh"));
	check(FPSMesh != nullptr);

	//this player sees this mesh.
	FPSMesh->SetOnlyOwnerSee(true);

	// attach  FPS mesh to FPS camera.
	FPSMesh->SetupAttachment(ShooterCameraComponent);

	// disable some environmental shadows 
	FPSMesh->bCastDynamicShadow = false;
	FPSMesh->CastShadow = false;

	// if multiplayer, others will see this mesh
	GetMesh()->SetOwnerNoSee(true);

	//Time is left
	isTimeLeft = true;
	//60 seconds in fact
	timer = 60.0f;
	//delay the end
	endTimer = 0.0f;
	//save delta Value
	tempDelta = 0;
}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	check(GEngine);

	// test message
	// The -1 "Key" value argument prevents the message from being updated or refreshed.
	//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, TEXT("We are using FPSCharacter."));
}

void AShooterCharacter::GameOver()
{
	if (isTimeLeft) {
		isTimeLeft = false;
	}
	endTimer += tempDelta;
	if (endTimer > 5.0f) {
		//OpenLevel("Title") restartps the game.
		UGameplayStatics::OpenLevel(this, "Title");
	}
		
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//tamporary delta Value
	tempDelta = DeltaTime;

	//Adjusts timer, or begins endgame if timer is through.
	if (timer > 0) {
		timer -= DeltaTime;
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString(TEXT("We are using FPSCharacter."),timer));
	}
	else {
		GameOver();
	}


}

// input functionality 
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// movement but he ain't using it.
	//PlayerInputComponent->BindAxis("MoveForward", this, &AShooterCharacter::MoveForward);
	//PlayerInputComponent->BindAxis("MoveRight", this, &AShooterCharacter::MoveRight);

	// camera control
	PlayerInputComponent->BindAxis("Turn", this, &AShooterCharacter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &AShooterCharacter::AddControllerPitchInput);
		
		
	// jump and fire
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AShooterCharacter::StartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AShooterCharacter::StopJump);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AShooterCharacter::Fire);
}
void AShooterCharacter::MoveForward(float Value)
{
    // determines the forward direction
    FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
    AddMovementInput(Direction, Value);
}
void AShooterCharacter::MoveRight(float Value)
{
    //determines the right direction.
    FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
    AddMovementInput(Direction, Value);
}

//We are not really using the jump eather, but I left it in, it's just a little obscure how to do it.  Kind of like an easter egg.
void AShooterCharacter::StartJump()
{
    bPressedJump = true;
}

void AShooterCharacter::StopJump()
{
    bPressedJump = false;
}

void AShooterCharacter::Fire()
{
	//This stops the gun from firing after 1 minute has passed, then 5 seconds later it loads the title screen
	if (isTimeLeft) {

		//  fire a projectile.
		if (ProjectileClass)
		{
			// camera transform.
			FVector CameraLocation;
			FRotator CameraRotation;
			GetActorEyesViewPoint(CameraLocation, CameraRotation);

			//  MuzzleOffset spawns projectiles in front of the camera.
			MuzzleOffset.Set(200.0f, 0.0f, 20.0f);
			FVector MuzzleLocation = CameraLocation + FTransform(CameraRotation).TransformVector(MuzzleOffset);

			// Skew the aim to be slightly upwards.
			FRotator MuzzleRotation = CameraRotation;
			MuzzleRotation.Pitch += 5.0f;

			UWorld* World = GetWorld();
			if (World)
			{
				FActorSpawnParameters SpawnParams;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				// Spawn the projectile near the muzzle.
				ABulletProjectile* Projectile = World->SpawnActor<ABulletProjectile>(ProjectileClass, MuzzleLocation, MuzzleRotation, SpawnParams);
				if (Projectile)
				{
					// Set the projectile's initial trajectory.
					FVector LaunchDirection = MuzzleRotation.Vector();
					Projectile->FireInDirection(LaunchDirection);
				}
			}
		}
	}
}